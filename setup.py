#!/usr/bin/env python

from setuptools import setup, find_packages


setup(  name="config_reader",
        version="0.1.2",
        author='Anthony Baire',
        author_email='Anthony.Baire@irisa.fr',
        license='LGPL3+',
        packages=find_packages(),
        install_requires=["PyYAML >= 3.0"],
        data_files=[],
        description='A schema-less lib for parsing yaml config files',
)

